<?php

namespace App\Http\Controllers;

use App\Models\Administrador;
use App\Models\Curso;
use App\Models\Estudiante;
use App\Models\EstudianteCurso;
use App\Models\Profesor;
use App\Models\Secretaria;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

class ControladorAdministrador extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        dd("hio");
        if (Auth::guard('web_administrador')->check()){

           $listadoEstudiantes = Estudiante::all();

           return view('administrador.index' ,compact('listadoEstudiantes'));

        }else{
            return redirect()->intended('inicio');
        }



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $objAdministrador = Administrador::create([
           'nombre' =>$request['nombre'],
           'apellido'=>$request['apellido'],
           'email' =>$request['email'],
           'password' =>$request['password'],

       ]);

       $objAdministrador->save();

       return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function listSecretarias()
    {
        $secretarias = Secretaria::all();
        return view('administrador.listadoSecretarias',compact('secretarias'));
    }

    public function listProfesores()
    {
        $profesores = Profesor::all();
        return view('administrador.listadoProfesores',compact('profesores'));
    }
    public function listCursos()
    {
        $objPofesores = Profesor::all();
        $cursos = Curso::all();

        return view('administrador.listadoCursos',compact('objPofesores','cursos'));

    }

    public function listCursosProfesor($idProfesor)
    {
        $listadoCursos = Curso::where('profesores_id',$idProfesor)->get();
        $profesor = Profesor::findOrFail($idProfesor);

        return view('administrador.listadoCursosProfesor',compact('listadoCursos','profesor'));
    }

    public function listCurEstu($id)
    {
        $objEstudiante = Estudiante::findOrFail($id);
        $cursos = Curso::all();
        $estudianteCursos = EstudianteCurso::where('estudiantes_id',$id)->get();

        return view('administrador.asignarCursos',compact('objEstudiante','cursos','estudianteCursos'));
    }

    public function asignarCurso(Request $request)
    {
        $buscador = EstudianteCurso::where([
            ['estudiantes_id',$request['estudiante']],
            ['cursos_id',$request['curso']]
        ])->get();

        if (count($buscador) == 0){

            $idAsignar = EstudianteCurso::max('id');

            if ($idAsignar == null ){

                $asignar = EstudianteCurso::create([
                    'id' => 1,
                    'estudiantes_id' => $request['estudiante'],
                    'cursos_id' => $request['curso'],
                ]);

            }elseif ($idAsignar >= 0){


                $asignar = EstudianteCurso::create([
                    'id' => $idAsignar+1,
                    'estudiantes_id' => $request['estudiante'],
                    'cursos_id' => $request['curso'],
                ]);
            }

            $asignar->save();

            return back()->with('flash','Se asigno correctamente el curso');
        }else{
            return back()->with('flash','El estudiante se encuentra registrado con anterioridad!');
        }
    }

    public function perfil()
    {
        return view('administrador.perfil');
    }

    public function editarPerfil(Request $request,$id)
    {
        if ($id == Auth::guard('web_administrador')->user()->id)
        {

            $objAdministrador = Administrador::findOrFail($id);

            $objAdministrador->nombre =$request['nombre'];
            $objAdministrador->apellido=$request['apellido'];

            $objAdministrador->email =$request['email'];

            if ($request['password'] != null){
                $objAdministrador->password =$request['password'];
            }

            $objAdministrador->save();

            return back()->with('flash','Servidor modificado Correctamente!');
        }else{
            return back()->with('flash','Error!');
        }
    }

}
