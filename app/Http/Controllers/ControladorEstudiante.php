<?php

namespace App\Http\Controllers;

use App\Models\Estudiante;
use Illuminate\Http\Request;

use App\Http\Requests;

class ControladorEstudiante extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $objEstudiante = Estudiante::create([
            'nombre' =>$request['nombre'],
            'apellido'=>$request['apellido'],
            'telefono'=>$request['telefono'],
            'num_documento'=>$request['cedula'],
            'tipo_documento' => $request['tipoDocumento'],
            'estrato'=>$request['estrato'],
            'direccion'=>$request['direccion'],
            'email' =>$request['email'],
            'password' =>$request['password'],
        ]);

        $objEstudiante->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if ($request->ajax()){

            $estudiante = Estudiante::findOrFail($id);
            return response()->json($estudiante);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax()){

            $objEstudiante = Estudiante::findOrFail($id);

            $objEstudiante->nombre = $request['modal-editar-estudiante-nombre'];
            $objEstudiante->apellido = $request['modal-editar-estudiante-apellido'];
            $objEstudiante->telefono = $request['modal-editar-estudiante-telefono'];
            $objEstudiante->direccion = $request['modal-editar-estudiante-direccion'];
            $objEstudiante->num_documento = $request['modal-editar-estudiante-cedula'];
            $objEstudiante->tipo_documento = $request['modal-editar-estudiante-tipoDocumento'];
            $objEstudiante->estrato = $request['modal-editar-estudiante-estrato'];
            $objEstudiante->email = $request['modal-editar-estudiante-email'];

            if ($request['modal-editar-estudiante-password'] != null){
                $objEstudiante->password = $request['modal-editar-estudiante-password'];
            }

            $objEstudiante->save();

            return response('Se actualizó correctamente', 200);
        }else{
            return response('No se pudo actualizar', 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
