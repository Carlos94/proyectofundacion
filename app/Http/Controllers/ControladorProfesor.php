<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use App\Models\EstudianteCurso;
use App\Models\Nota;
use App\Models\Profesor;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;

class ControladorProfesor extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listadoCursos = Curso::where('profesores_id',Auth::guard('web_profesor')->user()->id)->get();

       return view('profesor.index',compact('listadoCursos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $objProfesor = Profesor::create([
            'nombre' =>$request['nombre'],
            'apellido'=>$request['apellido'],
            'telefono'=>$request['telefono'],
            'email' =>$request['email'],
            'password' =>$request['password'],
        ]);

        $objProfesor->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if ($request->ajax())
        {
           $objProfesor = Profesor::findOrFail($id);

            return response()->json($objProfesor);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax())
        {
            $objProfesor = Profesor::findOrFail($id);

            $objProfesor->nombre =$request['modal-editar-profesor-nombre'];
            $objProfesor->apellido=$request['modal-editar-profesor-apellido'];
            $objProfesor->telefono=$request['modal-editar-profesor-telefono'];
            $objProfesor->email =$request['modal-editar-profesor-email'];

            if ($request['modal-editar-profesor-password'] != null){
                $objProfesor->password =$request['modal-editar-profesor-password'];
            }

            $objProfesor->save();

            return response('Se actualizó correctamente', 200);
        }else{
            return response('No se pudo actualizar', 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function perfil()
    {
        return view('profesor.perfil');
    }

    public function editarPerfil(Request $request,$id)
    {
        if ($id == Auth::guard('web_profesor')->user()->id)
        {

            $objProfesor = Profesor::findOrFail($id);

            $objProfesor->nombre =$request['nombre'];
            $objProfesor->apellido=$request['apellido'];
            $objProfesor->telefono=$request['telefono'];
            $objProfesor->email =$request['email'];

            if ($request['password'] != null){
                $objProfesor->password =$request['password'];
            }

            $objProfesor->save();

            return back()->with('flash','Profesor modificado Correctamente!');
        }else{
            return back()->with('flash','Error!');
        }
    }

    public function listadoEstudiantes($idCurso)
    {
        $listas = EstudianteCurso::where('cursos_id', $idCurso)->get();

        return view('profesor.listadoCurEstudiantes',compact('listas'));
    }

    public function agregarNota(Request $request)
    {
        $nota = Nota::create([
            'descripcion' => $request['descripcion'],
            'valor' => $request['valor'],
            'estudiantes_cursos_id' => $request['liNo'],
        ]);

        $nota->save();

        return back()->with('flash','Nota agregada!');;
    }

    public function listNotas($id)
    {
        $datosEstudiante = Nota::findOrFail($id);
        $notasEstudiante = Nota::where('estudiantes_cursos_id',$id)->get();

        //dd($notasEstudiante->estudianteCurso->estudiante->nombre);

        return view('profesor.listadoNotas',compact('notasEstudiante','datosEstudiante'));
    }

}
