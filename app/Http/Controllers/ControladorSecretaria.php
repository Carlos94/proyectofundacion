<?php

namespace App\Http\Controllers;

use App\Models\Secretaria;
use Illuminate\Http\Request;
use App\Models\Estudiante;
use App\Http\Requests;
use Auth;

class ControladorSecretaria extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::guard('web_secretaria')->check()){

            $listadoEstudiantes = Estudiante::all();

            return view('secretaria.index', compact('listadoEstudiantes'));

        }else{
            return redirect()->intended('inicio');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $objSecretaria = Secretaria::create([
            'nombre' =>$request['nombre'],
            'apellido'=>$request['apellido'],
            'email' =>$request['email'],
            'password' =>$request['password'],

        ]);

        $objSecretaria->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if ($request->ajax())
        {
            $objSecretaria = Secretaria::findOrFail($id);

            return response()->json($objSecretaria);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->ajax())
        {
            $objSecretaria = Secretaria::findOrFail($id);

            $objSecretaria->nombre =$request['modal-editar-secretaria-nombre'];
            $objSecretaria->apellido=$request['modal-editar-secretaria-apellido'];

            $objSecretaria->email =$request['modal-editar-secretaria-email'];

            if ($request['modal-editar-secretaria-password'] != null){
                $objSecretaria->password =$request['modal-editar-secretaria-password'];
            }

            $objSecretaria->save();

            return response('Se actualizó correctamente', 200);
        }else{
            return response('No se pudo actualizar', 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function perfil()
    {
        return view('secretaria.perfil');
    }

    public function editarPerfil(Request $request, $id)
    {
        if ($id == Auth::guard('web_secretaria')->user()->id)
        {

            $objSecretaria = Secretaria::findOrFail($id);

            $objSecretaria->nombre =$request['nombre'];
            $objSecretaria->apellido=$request['apellido'];

            $objSecretaria->email =$request['email'];

            if ($request['password'] != null){
                $objSecretaria->password =$request['password'];
            }

            $objSecretaria->save();

            return back()->with('flash','Servidor modificado Correctamente!');
        }else{
            return back()->with('flash','Error!');
        }
    }

}
