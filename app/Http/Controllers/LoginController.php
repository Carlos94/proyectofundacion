<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Redirect;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/inicio');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if(Auth::guard('web_administrador')->attempt(['email'=>$request['email'],'password'=>$request['password']])) {

            return redirect()->intended('administrador');

        }
        
        if(Auth::guard('web_estudiante')->attempt(['email'=>$request['email'],'password'=>$request['password']])) {

            return redirect()->intended('estudiante');
        }

        if(Auth::guard('web_secretaria')->attempt(['email'=>$request['email'],'password'=>$request['password']])) {

            return redirect()->intended('secretaria');
        }
        if(Auth::guard('web_profesor')->attempt(['email'=>$request['email'],'password'=>$request['password']])) {

            return redirect()->intended('profesor');
        }

        return redirect()->back();
    }

    public function logout()
    {

        if (Auth::guard('web_administrador')->check()) {
            Auth::guard('web_administrador')->logout();
        }

        if (Auth::guard('web_estudiante')->check()) {
            Auth::guard('web_estudiante')->logout();
        }
        if (Auth::guard('web_profesor')->check()) {
            Auth::guard('web_profesor')->logout();
        }

        if (Auth::guard('web_secretaria')->check()) {
            Auth::guard('web_secretaria')->logout();
        }
        return redirect()->to('log');
    }


}
