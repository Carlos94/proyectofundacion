<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('inicio', function () {
    return view('login.inicio');
});

Route::get('forAdmin', function () {
    return view('administrador.modals.modal-agregar-administrador');
});

Route::get('forEstu', function () {
    return view('administrador.modals.modal-agregar-estudiante');
});

Route::get('forPro', function () {
    return view('administrador.modals.modal-agregar-profesor');
});

Route::get('forCur', function () {
    $objPofesores = \App\Models\Profesor::all();
    return view('administrador.modals.modal-agregar-curso',compact('objPofesores'));
});

Route::get('fo', function () {
    return view('layout.dashboard');
});

Route::resource('log','LoginController');

Route::get('logout','LoginController@logout');

Route::get('perfil-administrador','ControladorAdministrador@perfil');
Route::PUT('perfilAdministrador/{id}',['uses' => 'ControladorAdministrador@editarPerfil', 'as' => 'perfilAdministrador']);
Route::get('listado-profesores','ControladorAdministrador@listProfesores');
Route::get('listado-secretarias','ControladorAdministrador@listSecretarias');
Route::get('listado-de-cursos','ControladorAdministrador@listCursos');
Route::get('listado-de-cursos-profesor/{idProfesor}',['uses' => 'ControladorAdministrador@listCursosProfesor', 'as' => 'listado-de-cursos-profesor']);
Route::get('administrador/listado-cursos-estudiante/{id}',['uses' => 'ControladorAdministrador@listCurEstu', 'as' => 'listEstuCurso']);
Route::POST('asignarCurso', ['uses' => 'ControladorAdministrador@asignarCurso', 'as'=>'asignarCurso']);
Route::resource('administrador','ControladorAdministrador');


Route::get('perfil-secretaria','ControladorSecretaria@perfil');
Route::PUT('perfilSecretaria/{id}',['uses' => 'ControladorSecretaria@editarPerfil', 'as' => 'perfilSecretaria']);
Route::resource('secretaria','ControladorSecretaria');



Route::get('perfil-profesor','ControladorProfesor@perfil');
Route::PUT('perfilProfesor/{id}',['uses' => 'ControladorProfesor@editarPerfil', 'as' => 'perfilProfesor']);
Route::get('profesor/listado/{idCurso}',['uses' => 'ControladorProfesor@listadoEstudiantes', 'as' => 'profesor/listado']);
Route::POST('profesor/listado/notas',['uses' => 'ControladorProfesor@agregarNota', 'as' => 'profesor/listado/notas']);
Route::get('profesor/notas/{id}',['uses' => 'ControladorProfesor@listNotas', 'as'=>'listNotas']);
Route::resource('profesor','ControladorProfesor');


Route::resource('estudiante','ControladorEstudiante');

Route::resource('curso','ControladorCurso');

