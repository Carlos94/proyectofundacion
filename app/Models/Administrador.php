<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class Administrador extends Authenticatable
{
    protected $table = "administradores";

    protected $fillable = [
        'nombre',
        'apellido',
        'email',
        'password',
        'remember_token'
    ];

    protected $hidden = ['password'];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = mb_strtolower($value, 'UTF-8');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
