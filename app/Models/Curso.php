<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    protected $table = "cursos";

    protected $fillable = [
        'nombre',
        'creditos',
        'duracion',
        'profesores_id',
    ];

    public function profesor(){

        return $this->belongsTo('App\Models\Profesor','profesores_id');
    }


}
