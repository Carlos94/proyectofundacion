<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;


class Estudiante extends Authenticatable
{
    protected $table = "estudiantes";

    protected $fillable = [
        'nombre',
        'apellido',
        'num_documento',
        'tipo_documento',
        'telefono',
        'direccion',
        'estrato',
        'email',
        'password',
        'remember_token'
    ];

    protected $hidden = ['password'];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = mb_strtolower($value, 'UTF-8');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function notas()
    {
        return $this->hasMany('App\Models\Nota');
    }
}
