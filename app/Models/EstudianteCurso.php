<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EstudianteCurso extends Model
{
    protected $table = "estudiantes_cursos";

    protected $fillable = [
        'estudiantes_id',
        'cursos_id',
        'id',

    ];

    public function estudiante(){

        return $this->belongsTo('App\Models\Estudiante','estudiantes_id');
    }

    public function curso(){

        return $this->belongsTo('App\Models\Curso','cursos_id');
    }

}
