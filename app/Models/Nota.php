<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{

    protected $table = "notas";

    protected $fillable = [
        'descripcion',
        'valor',
        'estudiantes_cursos_id',
    ];
    public function estudianteCurso()
    {
        return $this->belongsTo('App\Models\EstudianteCurso','estudiantes_cursos_id');
    }


}
