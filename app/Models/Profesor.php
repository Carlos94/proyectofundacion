<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;

class Profesor extends Authenticatable
{
    protected $table = "profesores";

    protected $fillable = [
        'nombre',
        'apellido',
        'telefono',
        'email',
        'password',
        'remember_token'
    ];

    protected $hidden = ['password'];

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = mb_strtolower($value, 'UTF-8');
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function cursos()
    {
        return $this->hasMany('App\Models\Curso');
    }

}
