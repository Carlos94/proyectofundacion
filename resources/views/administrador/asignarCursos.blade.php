@extends('layout.dashboard')

@section('titulo')
Datos del Estudiantes
@endsection

@section('boton')
      <a type="button" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light"
          href="/administrador"  >Regresar</a>
@endsection

@section('contenido')


 <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="nombre" >Nombre</label>
                    <input type="text" id="nombre" name="nombre"  class="form-control" required="required"
                           value="{{$objEstudiante->nombre}}" disabled="true">
                </div>
                <div class="form-group">
                    <label for="email" >Correo</label>
                    <input type="email" id="email" name="email"  class="form-control" required="required"
                           value="{{$objEstudiante->email}}" disabled="true">
                </div>
                <div class="form-group">
                    <label for="email" >Telefono</label>
                    <input type="number" id="telefono" name="telefono"  class="form-control" required="required"
                           value="{{$objEstudiante->telefono}}" disabled="true">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="apellido" >Apellido </label>
                    <input type="text" id="apellido" name="apellido" required="required"  class="form-control"
                           value="{{$objEstudiante->apellido}}" disabled="true">
                </div>

                <div class="form-group">
                    <label for="contra" >Documento de Identidad</label>
                    <input id="documento" type="number" name="documento"  class="form-control" value="{{$objEstudiante->num_documento}}" disabled="true">
                </div>
            </div>

        </div>

<br>
 <div class="modal-footer"></div>

<h3  class="box-title">Cursos del Estudiantes</h3>

<div>
    
 <form action="{{url('asignarCurso')}}" method="POST" class="form-inline" autocomplete="off" id="addCurso" >
     <div class="modal-body">
         {{ csrf_field() }}
         <div class="row">
             <div class="">
                 <input type="hidden" value="{{$objEstudiante->id}}" id="estudiante" name="estudiante">
                 <label for="nombre" style="font-weight: 700">Curso</label>
                 <select id="curso" name="curso" class="form-control">
                     @foreach($cursos as $curso)
                         <option value="{{$curso->id}}"> {{$curso->nombre}}</option>
                     @endforeach

                 </select>
             </div>
         </div>
     </div>
     <button type="submit" class="btn btn-info">Agregar</button>
 </form>

</div>

 <br>
    <div class="table-responsive">
        <table id="listado-estudiantes" class="table table-bordered">
            <thead>
            <tr>

                <th>Nombre.</th>
                <th>Duracion</th>

            </tr>
            </thead>
           @foreach($estudianteCursos as $cursoEstudiante)
                <tr data-id="{{$cursoEstudiante->id}}">
                    <td>{{$cursoEstudiante->curso->nombre}}</td>
                    <td>{{$cursoEstudiante->curso->duracion}}</td>

            @endforeach
           
        </table>
    </div>
@endsection

@section('script')

    <script type="text/javascript">
        

        $('.editar-estudiante').on('click', function (e) {
            e.preventDefault();
            var fila = $(this).parents('tr');
            var id = fila.data('id');
            $.ajax({
                type: 'GET',
                url: 'estudiante/' + id,
                success: function (data) {

                    console.log(data);
                    $('#modal-editar-estudiante-id').val(data.id);
                    $('#modal-editar-estudiante-nombre').val(data.nombre);
                    $('#modal-editar-estudiante-apellido').val(data.apellido);
                    $('#modal-editar-estudiante-cedula').val(data.num_documento);
                    $('#modal-editar-estudiante-telefono').val(data.telefono);
                    $('#modal-editar-estudiante-direccion').val(data.direccion);
                    $('#modal-editar-estudiante-email').val(data.email);
                    $('select[id="modal-editar-estudiante-estrato"]').val(data.estrato);
                    $('select[id="modal-editar-estudiante-tipoDocumento"]').val(data.tipo_documento);
                    $("#modal-editar-estudiante").modal('toggle');
                }
            });
        });

        $('#editEstudiante').on('submit', function (e) {
            e.preventDefault();
            var id=$("#modal-editar-estudiante-id").val();

            $.ajax({
                type: 'PUT',
                url: 'estudiante/'+id,
                data: $('#editEstudiante').serialize(),
                success: function(data, textStatus, jqXHR){

                    $('#modal-editar-estudiante').modal('hide');
//
                    document.getElementById("editEstudiante").reset();

                    swal("Estudiante", data, "success");

                    location.reload();
                },
                error: function(data, textStatus, errorThrown) {
                    swal("Estudiante", data, "error");
                }
            });
        });

    </script>

@endsection

