@extends('layout.dashboard')

@section('titulo')
Listado de Estudiantes
@endsection

@section('boton')
    <button type="button" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light" data-toggle="modal" data-target="#modal-agregar-estudiante">Agregar Estudiante</button>
@endsection

@section('contenido')
    @include('administrador.modals.modal-agregar-estudiante')
    @include('administrador.modals.modal-editar-estudiante')

<br>
    <div class="table-responsive">
        <table id="listado-estudiantes" class="table table-bordered">
            <thead>
            <tr>

                <th>Nombre.</th>
                <th>Apellido.</th>
                <th>Tipo Documento.</th>
                <th>No. Identidad.</th>
                <th>Correo.</th>
                <th>Opciones.</th>

            </tr>
            </thead>
            @foreach($listadoEstudiantes as $estudiante)
                <tr data-id="{{$estudiante->id}}">
                    <td>{{$estudiante->nombre}}</td>
                    <td>{{$estudiante->apellido}}</td>
                    <td>{{$estudiante->tipo_documento}}</td>
                    <td>{{$estudiante->num_documento}}</td>
                    <td>{{$estudiante->email}}</td>
                    <td>
                        <button class="btn btn-danger editar-estudiante">Editar</button>
                        {!! link_to_route('listEstuCurso', $title ="Asignar Cursos", $parameters = $estudiante->id,
                         $attributes = ['class'=> 'btn  btn-danger'])!!}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function() {

            $('#listado-estudiantes').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"
                }
            });

        });

        $('.editar-estudiante').on('click', function (e) {
            e.preventDefault();
            var fila = $(this).parents('tr');
            var id = fila.data('id');
            $.ajax({
                type: 'GET',
                url: 'estudiante/' + id,
                success: function (data) {

                    console.log(data);
                    $('#modal-editar-estudiante-id').val(data.id);
                    $('#modal-editar-estudiante-nombre').val(data.nombre);
                    $('#modal-editar-estudiante-apellido').val(data.apellido);
                    $('#modal-editar-estudiante-cedula').val(data.num_documento);
                    $('#modal-editar-estudiante-telefono').val(data.telefono);
                    $('#modal-editar-estudiante-direccion').val(data.direccion);
                    $('#modal-editar-estudiante-email').val(data.email);
                    $('select[id="modal-editar-estudiante-estrato"]').val(data.estrato);
                    $('select[id="modal-editar-estudiante-tipoDocumento"]').val(data.tipo_documento);
                    $("#modal-editar-estudiante").modal('toggle');
                }
            });
        });

        $('#editEstudiante').on('submit', function (e) {
            e.preventDefault();
            var id=$("#modal-editar-estudiante-id").val();

            $.ajax({
                type: 'PUT',
                url: 'estudiante/'+id,
                data: $('#editEstudiante').serialize(),
                success: function(data, textStatus, jqXHR){

                    $('#modal-editar-estudiante').modal('hide');
//
                    document.getElementById("editEstudiante").reset();

                    swal("Estudiante", data, "success");

                    location.reload();
                },
                error: function(data, textStatus, errorThrown) {
                    swal("Estudiante", data, "error");
                }
            });
        });

    </script>

@endsection

