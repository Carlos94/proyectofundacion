@extends('layout.dashboard')

@section('titulo')
    Listado de Cursos
@endsection

@section('boton')
    <button type="button" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light" data-toggle="modal"
            data-target="#modal-agregar-curso">Agregar Curso</button>
@endsection

@section('contenido')
    @include('administrador.modals.modal-agregar-curso')


    <br>
    <div class="table-responsive">
        <table id="listado-cursos" class="table table-bordered">
            <thead>
            <tr>

                <th>Nombre.</th>
                <th>Creditos.</th>
                <th>Duracion.</th>
                <th>Profesor.</th>
                <th>Opciones.</th>

            </tr>
            </thead>
            @foreach($cursos as $curso)
                <tr data-id="{{$curso->id}}">
                    <td>{{$curso->nombre}}</td>
                    <td>{{$curso->creditos}}</td>
                    <td>{{$curso->duracion}}</td>
                    <td>{{$curso->profesor->nombre}}</td>
                    <td><button class="btn btn-danger editar-estudiante">Editar</button></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function() {

            $('#listado-cursos').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"
                }
            });

        });

    </script>

@endsection

