@extends('layout.dashboard')

@section('titulo')
    Cursos del tutor: {{$profesor->nombre}}
@endsection

@section('boton')
    <a type="button" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light"
          href="/listado-profesores"  >Regresar</a>

@endsection

@section('contenido')

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="nombre" >Nombre</label>
                <input type="text" id="nombre" name="nombre"  class="form-control" required="required"
                       value="{{$profesor->nombre}}" disabled>
            </div>
            <div class="form-group">
                <label for="email" >Correo</label>
                <input type="text" id="email" name="email"  class="form-control" required="required"
                       value="{{$profesor->email}}" disabled>
            </div>


        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="apellido" >Apellido </label>
                <input type="text" id="apellido" name="apellido" required="required"  class="form-control"
                       value="{{$profesor->apellido}}" disabled>
            </div>

            <div class="form-group">
                <label for="contra" >Telefono</label>
                <input id="telefono" type="text" name="telefono"  class="form-control" value="{{$profesor->telefono}}" disabled>
            </div>
        </div>

    </div>


    <br>
    <div class="table-responsive">
        <table id="listado-curProfesor" class="table table-bordered">
            <thead>
            <tr>

                <th>Nombre</th>
                <th>Creditos</th>
                <th>Duracion</th>

            </tr>
            </thead>
            @foreach($listadoCursos as $curso)
                <tr data-id="{{$curso->id}}">
                    <td>{{$curso->nombre}}</td>
                    <td>{{$curso->creditos}}</td>
                    <td>{{$curso->duracion}}</td>

                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function() {



        });


    </script>

@endsection

