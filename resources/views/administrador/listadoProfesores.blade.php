@extends('layout.dashboard')

@section('titulo')
    Listado de Profesores
@endsection

@section('boton')
    <button type="button" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light" data-toggle="modal"
            data-target="#modal-agregar-profesor">Agregar Profesor</button>
@endsection

@section('contenido')
    @include('administrador.modals.modal-agregar-profesor')
    @include('administrador.modals.modal-editar-profesor')

    <br>
    <div class="table-responsive">
        <table id="listado-profesores" class="table table-bordered">
            <thead>
            <tr>

                <th>Nombre.</th>
                <th>Apellido.</th>
                <th>Telefono.</th>
                <th>Correo.</th>
                <th>Opciones.</th>

            </tr>
            </thead>
            @foreach($profesores as $profesor)
                <tr data-id="{{$profesor->id}}">
                    <td>{{$profesor->nombre}}</td>
                    <td>{{$profesor->apellido}}</td>
                    <td>{{$profesor->telefono}}</td>
                    <td>{{$profesor->email}}</td>
                    <td>
                        <button class="btn btn-danger editar-estudiante">Editar</button>
                        {!! link_to_route('listado-de-cursos-profesor', $title ="Ver Cursos", $parameters = $profesor->id,
                         $attributes = ['class'=> 'btn  btn-danger'])!!}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function() {

            $('#listado-profesores').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"
                }
            });
        });


        $('.editar-profesor').on('click', function (e) {
            e.preventDefault();
            var fila = $(this).parents('tr');
            var id = fila.data('id');
            $.ajax({
                type: 'GET',
                url: 'profesor/' + id,
                success: function (data) {
                    console.log(data);

                    $('#modal-editar-profesor-id').val(data.id);
                    $('#modal-editar-profesor-nombre').val(data.nombre);
                    $('#modal-editar-profesor-apellido').val(data.apellido);
                    $('#modal-editar-profesor-telefono').val(data.telefono);
                    $('#modal-editar-profesor-email').val(data.email);
                    $("#modal-editar-profesor").modal('toggle');
                }
            });
        });

        $('#editProfesor').on('submit', function (e) {
            e.preventDefault();
            var id=$("#modal-editar-profesor-id").val();

            $.ajax({
                type: 'PUT',
                url: 'profesor/'+id,
                data: $('#editProfesor').serialize(),
                success: function(data, textStatus, jqXHR){

                    $('#modal-editar-profesor').modal('hide');
//
                    document.getElementById("editProfesor").reset();

                    swal("Profesor", data, "success");

                    location.reload();
                },
                error: function(data, textStatus, errorThrown) {
                    swal("Profesor", data, "error");
                }
            });
        });

    </script>

@endsection

