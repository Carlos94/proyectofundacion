@extends('layout.dashboard')

@section('titulo')
    Listado de Secretarias
@endsection

@section('boton')
    <button type="button" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light" data-toggle="modal" data-target="#modal-agregar-secretaria">Agregar Secretaria</button>
@endsection

@section('contenido')
    @include('administrador.modals.modal-agregar-secretaria')
    @include('administrador.modals.modal-editar-secretaria')


    <br>
    <div class="table-responsive">
        <table id="listado-secretarias" class="table table-bordered">
            <thead>
            <tr>

                <th>Nombre.</th>
                <th>Apellido.</th>
                <th>Correo.</th>
                <th>Opciones.</th>

            </tr>
            </thead>
            @foreach($secretarias as $secretaria)
                <tr data-id="{{$secretaria->id}}">
                    <td>{{$secretaria->nombre}}</td>
                    <td>{{$secretaria->apellido}}</td>
                    <td>{{$secretaria->email}}</td>
                    <td><button class="btn btn-danger editar-secretaria">Editar</button></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('script')

    <script type="text/javascript">
        $(document).ready(function() {

            $('#listado-secretaria').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"
                }
            });

        });

        $('.editar-secretaria').on('click', function (e) {
            e.preventDefault();
            var fila = $(this).parents('tr');
            var id = fila.data('id');
            $.ajax({
                type: 'GET',
                url: 'secretaria/' + id,
                success: function (data) {
                    console.log(data);

                    $('#modal-editar-secretaria-id').val(data.id);
                    $('#modal-editar-secretaria-nombre').val(data.nombre);
                    $('#modal-editar-secretaria-apellido').val(data.apellido);
                    $('#modal-editar-secretaria-email').val(data.email);
                    $("#modal-editar-secretaria").modal('toggle');
                }
            });
        });

        $('#editSecretaria').on('submit', function (e) {
            e.preventDefault();
            var id=$("#modal-editar-secretaria-id").val();

            $.ajax({
                type: 'PUT',
                url: 'secretaria/'+id,
                data: $('#editSecretaria').serialize(),
                success: function(data, textStatus, jqXHR){

                    $('#modal-editar-secretaria').modal('hide');
//
                    document.getElementById("editSecretaria").reset();

                    swal("Secretaria", data, "success");

                    location.reload();
                },
                error: function(data, textStatus, errorThrown) {
                    swal("Secretaria", data, "error");
                }
            });
        });

    </script>

@endsection

