<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-agregar-curso">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header" align="center" style="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Agregar Curso</h4>
            </div>

            <form action="{{url('curso')}}" method="POST" class="" autocomplete="off" id="registroCurso">
                <div class="modal-body">

                    {{ csrf_field() }}

                    <div class="row">
                        <div class="form-group">
                            <label for="nombre" style="font-weight: 700">Nombre</label>
                            <input  type="text" name="nombre" id="nombre"  class="form-control col-md-7 col-xs-12"placeholder="Nombre del curso">
                        </div>
                        <div class="form-group">
                            <label for="creditos">Creditos</label>
                            <input type="number" class="form-control" id="creditos" name="creditos"  class="form-control col-md-7 col-xs-12"placeholder="Creditos del curso">
                        </div>
                        <div class="form-group">
                            <label for="duracion">Duracion</label>
                            <input type="text" id="duracion" class="form-control" name="duracion"  class="form-control col-md-7 col-xs-12"placeholder="Duracion del curso">
                        </div>
                        <div class="form-group">
                            <label for="profesor">Profesor</label>
                           <select name="profesor" id="profesor" class="form-control col-md-7 col-xs-12">
                               <option selected disabled value="">Seleccione el profesor</option>
                                @foreach($objPofesores as $profesor)
                                   <option value="{{$profesor->id}}">{{$profesor->nombre}} {{$profesor->apellido}}</option>
                                    @endforeach
                           </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-info">Aceptar</button>
                </div>
            </form>


        </div>
    </div>
</div>

@push('script')
    <script type="text/javascript">
        //        $('#registroTutoria').on('submit', function (e) {
        //            e.preventDefault();
        //
        //            $.ajax({
        //                type: 'POST',
        //                url: 'agregarTutoria',
        //                data: $('#registroTutoria').serialize(),
        //                success: function () {
        //
        //                    swal("Tutorias", "Tu tutoria se ha creado con exito!!","success");
        //
        //                    location.reload();
        //
        //                },
        //                error: function (data) {
        //                    console.log(data.responseText);
        //                }
        //            });
        //        });
    </script>
@endpush