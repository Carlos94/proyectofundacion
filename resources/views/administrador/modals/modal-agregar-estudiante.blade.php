<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-agregar-estudiante">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header" align="center" style="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Agregar Estudiante</h4>
            </div>

            <form action="{{url('estudiante')}}" method="POST" class="" autocomplete="off" id="registroEstudiante">
                <div class="modal-body">

                    {{ csrf_field() }}

                    <div class="row">
                        <div class="form-group">
                            <label for="nombre" style="font-weight: 700">Nombre</label>
                            <input  type="text" name="nombre" id="nombre"  class="form-control col-md-7 col-xs-12" placeholder="Nombre del estudiante">
                        </div>
                        <div class="form-group">
                            <label for="apellido"  style="font-weight: 700">Apellido</label>
                            <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido del estudiante" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="tipo"  style="font-weight: 700">Tipo Documento</label>
                            <select name="tipoDocumento" id="tipoDocumento" class="form-control col-md-7 col-xs-12">
                                <option selected disabled="true">Seleccione un tipo de documento</option>
                                <option value="Tarjeta de identidad">Tarjeta de identidad</option>
                                <option value="Cedula ciudadania">Cédula de ciudadania</option>
                                <option value="Cedula extranjeria">Cédula de extranjería</option>
                                <option value="contrasenia">Contraseña registraduria</option>
                                <option value="Pasaporte colombiano">Pasaporte Colombiano</option>
                                <option value="Pasaporte extranjero">Pasaporte extranjero</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="correo"  style="font-weight: 700">Num. Documento</label>
                            <input type="number" id="cedula" class="form-control" name="cedula" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="correo"  style="font-weight: 700">Telefono</label>
                            <input type="number" id="telefono" class="form-control" name="telefono" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="correo"  style="font-weight: 700">Direccion</label>
                            <input type="text" id="direccion" class="form-control" name="direccion" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="correo"  style="font-weight: 700">Estrato</label>
                            <select name="estrato" id="estrato" class="form-control col-md-7 col-xs-12">
                                <option selected disabled="true">Seleccione un estrato</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="correo"  style="font-weight: 700">Correo</label>
                            <input type="email" id="email" class="form-control" name="email" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="contrania"  style="font-weight: 700">Contraseña</label>
                            <input type="password" id="password" class="form-control" name="password" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-info">Aceptar</button>
                </div>
            </form>


        </div>
    </div>
</div>

@push('script')
    <script type="text/javascript">
        //        $('#registroTutoria').on('submit', function (e) {
        //            e.preventDefault();
        //
        //            $.ajax({
        //                type: 'POST',
        //                url: 'agregarTutoria',
        //                data: $('#registroTutoria').serialize(),
        //                success: function () {
        //
        //                    swal("Tutorias", "Tu tutoria se ha creado con exito!!","success");
        //
        //                    location.reload();
        //
        //                },
        //                error: function (data) {
        //                    console.log(data.responseText);
        //                }
        //            });
        //        });
    </script>
@endpush