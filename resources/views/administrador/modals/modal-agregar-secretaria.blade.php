<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-agregar-secretaria">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header" align="center" style="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Agregar Secretaria!</h4>
            </div>

            <form action="{{url('secretaria')}}" method="POST" class="" autocomplete="off" id="registroEstudiante">
                <div class="modal-body">

                    {{ csrf_field() }}

                    <div class="row">
                        <div class="form-group">
                            <label for="nombre" style="font-weight: 700">Nombre</label>
                            <input  type="text" name="nombre" id="nombre" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="apellido">Apellido</label>
                            <input type="text" class="form-control" id="apellido" name="apellido" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="correo">Correo</label>
                            <input type="email" id="email" class="form-control" name="email"vclass="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="contrania">Contraseña</label>
                            <input type="password" id="password" class="form-control" name="password"vclass="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-info">Aceptar</button>
                </div>
            </form>


        </div>
    </div>
</div>

@push('script')
    <script type="text/javascript">
        //        $('#registroTutoria').on('submit', function (e) {
        //            e.preventDefault();
        //
        //            $.ajax({
        //                type: 'POST',
        //                url: 'agregarTutoria',
        //                data: $('#registroTutoria').serialize(),
        //                success: function () {
        //
        //                    swal("Tutorias", "Tu tutoria se ha creado con exito!!","success");
        //
        //                    location.reload();
        //
        //                },
        //                error: function (data) {
        //                    console.log(data.responseText);
        //                }
        //            });
        //        });
    </script>
@endpush