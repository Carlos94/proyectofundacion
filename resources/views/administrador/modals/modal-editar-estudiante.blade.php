<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-editar-estudiante">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header" align="center" style="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Editar Estudiante</h4>
            </div>

            <form action="{{url('estudiante')}}" method="POST" class="" autocomplete="off" id="editEstudiante">
                <div class="modal-body">

                    {{ csrf_field() }}
                    <input type="hidden" value="" id="modal-editar-estudiante-id" name="modal-editar-estudiante-id">

                    <div class="row">
                        <div class="form-group">
                            <label for="nombre" style="font-weight: 700">Nombre</label>
                            <input  type="text" name="modal-editar-estudiante-nombre" id="modal-editar-estudiante-nombre"  class="form-control col-md-7 col-xs-12" placeholder="Nombre del estudiante">
                        </div>
                        <div class="form-group">
                            <label for="apellido"  style="font-weight: 700">Apellido</label>
                            <input type="text" class="form-control" id="modal-editar-estudiante-apellido" name="modal-editar-estudiante-apellido" placeholder="Apellido del estudiante" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="tipo"  style="font-weight: 700">Tipo Documento</label>
                            <select name="modal-editar-estudiante-tipoDocumento" id="modal-editar-estudiante-tipoDocumento" class="form-control col-md-7 col-xs-12">
                                <option selected disabled="true">Seleccione un tipo de documento</option>
                                <option value="Tarjeta de identidad">Tarjeta de identidad</option>
                                <option value="Cedula ciudadania">Cédula de ciudadania</option>
                                <option value="Cedula extranjeria">Cédula de extranjería</option>
                                <option value="contrasenia">Contraseña registraduria</option>
                                <option value="Pasaporte colombiano">Pasaporte Colombiano</option>
                                <option value="Pasaporte extranjero">Pasaporte extranjero</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="cedula"  style="font-weight: 700">Cedula</label>
                            <input type="number" id="modal-editar-estudiante-cedula" class="form-control" name="modal-editar-estudiante-cedula" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="correo"  style="font-weight: 700">Telefono</label>
                            <input type="number" id="modal-editar-estudiante-telefono" class="form-control" name="modal-editar-estudiante-telefono" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="correo"  style="font-weight: 700">Direccion</label>
                            <input type="text" id="modal-editar-estudiante-direccion" class="form-control" name="modal-editar-estudiante-direccion" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="correo"  style="font-weight: 700">Estrato</label>
                            <select name="modal-editar-estudiante-estrato" id="modal-editar-estudiante-estrato" class="form-control col-md-7 col-xs-12">
                                <option selected disabled="true">Seleccione un estrato</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="correo"  style="font-weight: 700">Correo</label>
                            <input type="email" id="modal-editar-estudiante-email" class="form-control" name="modal-editar-estudiante-email" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="contrania"  style="font-weight: 700">Contraseña</label>
                            <input type="password" id="modal-editar-estudiante-password" class="form-control" name="modal-editar-estudiante-password" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-info">Aceptar</button>
                </div>
            </form>


        </div>
    </div>
</div>

