<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-editar-profesor">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header" align="center" style="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Editar Profesor!</h4>
            </div>

            <form action="{{url('profesor')}}" method="POST" class="" autocomplete="off" id="editProfesor">
                <div class="modal-body">

                    {{ csrf_field() }}

                    <input type="hidden" value="" id="modal-editar-profesor-id" name="modal-editar-profesor-id">

                    <div class="row">
                        <div class="form-group">
                            <label for="nombre" style="font-weight: 700">Nombre</label>
                            <input  type="text" name="modal-editar-profesor-nombre" id="modal-editar-profesor-nombre" class="form-control col-md-7 col-xs-12" placeholder="Coloque el nombre del adminsitrador">
                        </div>
                        <div class="form-group">
                            <label for="apellido">Apellido</label>
                            <input type="text" class="form-control col-md-7 col-xs-12" id="modal-editar-profesor-apellido" name="modal-editar-profesor-apellido" placeholder="apellido">
                        </div>
                        <div class="form-group">
                            <label for="correo">Telefono</label>
                            <input type="number" id="modal-editar-profesor-telefono" class="form-control col-md-7 col-xs-12" name="modal-editar-profesor-telefono">
                        </div>
                        <div class="form-group">
                            <label for="correo">Correo</label>
                            <input type="email" id="modal-editar-profesor-email" class="form-control col-md-7 col-xs-12" name="modal-editar-profesor-email">
                        </div>
                        <div class="form-group">
                            <label for="contrania">Contraseña</label>
                            <input type="password" id="modal-editar-profesor-password" class="form-control col-md-7 col-xs-12" name="modal-editar-profesor-password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-info">Aceptar</button>
                </div>
            </form>


        </div>
    </div>
</div>

@push('script')
    <script type="text/javascript">
        //        $('#registroTutoria').on('submit', function (e) {
        //            e.preventDefault();
        //
        //            $.ajax({
        //                type: 'POST',
        //                url: 'agregarTutoria',
        //                data: $('#registroTutoria').serialize(),
        //                success: function () {
        //
        //                    swal("Tutorias", "Tu tutoria se ha creado con exito!!","success");
        //
        //                    location.reload();
        //
        //                },
        //                error: function (data) {
        //                    console.log(data.responseText);
        //                }
        //            });
        //        });
    </script>
@endpush