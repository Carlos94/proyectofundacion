<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-editar-secretaria">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header" align="center" style="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Editar Secretaria</h4>
            </div>

            <form action="{{url('secretaria')}}" method="POST" class="" autocomplete="off" id="editSecretaria">
                <div class="modal-body">

                    {{ csrf_field() }}

                    <input type="hidden" id="modal-editar-secretaria-id" name="modal-editar-secretaria-id">
                    <div class="row">
                        <div class="form-group">
                            <label for="nombre" style="font-weight: 700">Nombre</label>
                            <input  type="text" name="modal-editar-secretaria-nombre" id="modal-editar-secretaria-nombre" class="form-control col-md-7 col-xs-12">
                        </div>
                        <div class="form-group">
                            <label for="apellido">Apellido</label>
                            <input type="text" class="form-control col-md-7 col-xs-12" id="modal-editar-secretaria-apellido" name="modal-editar-secretaria-apellido">
                        </div>
                        <div class="form-group">
                            <label for="correo">Correo</label>
                            <input type="email" id="modal-editar-secretaria-email" class="form-control col-md-7 col-xs-12" name="modal-editar-secretaria-email">
                        </div>
                        <div class="form-group">
                            <label for="contrania">Contraseña</label>
                            <input type="password" id="modal-editar-secretaria-password" class="form-control col-md-7 col-xs-12" name="modal-editar-secretaria-password">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-info">Aceptar</button>
                </div>
            </form>


        </div>
    </div>
</div>

@push('script')
    <script type="text/javascript">

    </script>
@endpush