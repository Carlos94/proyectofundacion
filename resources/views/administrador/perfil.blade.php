@extends('layout.dashboard')

@section('contenido')
    <div class="content box">
        <h2 class="titulo-section">Perfil de Usuario</h2>

        @if(session()->has('flash'))

            <div class="container">
                {{--<div class="alert alert-success">{{session('flash')}}</div>--}}
            </div>
        @endif

        {!! Form::model(Auth::guard('web_administrador')->user()->id,['route'=>['perfilAdministrador',Auth::guard('web_administrador')->user()->id], 'method'=>'PUT','id'=>'perfilAdmin']) !!}

        {{ csrf_field() }}
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="nombre" >Nombre</label>
                    <input type="text" id="nombre" name="nombre"  class="form-control" required="required"
                           value="{{Auth::guard('web_administrador')->user()->nombre}}">
                </div>
                <div class="form-group">
                    <label for="email" >Correo</label>
                    <input type="email" id="email" name="email"  class="form-control" required="required"
                           value="{{Auth::guard('web_administrador')->user()->email}}">
                </div>

            </div>
            <div class="col-xs-12 col-sm-6">
                <div class="form-group">
                    <label for="apellido" >Apellido </label>
                    <input type="text" id="apellido" name="apellido" required="required"  class="form-control"
                           value="{{Auth::guard('web_administrador')->user()->apellido}}">
                </div>

                <div class="form-group">
                    <label for="contra" >Nueva Contraseña</label>
                    <input id="password" type="password" name="password"  class="form-control">
                </div>
            </div>

        </div>
        <div class="form-footer box" align="right">
            <button type="submit" class="btn btn-danger">Guardar</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection


