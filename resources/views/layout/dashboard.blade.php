<!DOCTYPE html>
<html lang="en">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Luis Salgado">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/logo.png')}}">
    <title>Dashboard - Fundacion Amanecer</title>

    <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/sidebar-nav.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/jquery.toast.css')}}" rel="stylesheet">
    <link href="{{asset('css/morris.css')}}" rel="stylesheet">

    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/default.css')}}" id="theme" rel="stylesheet">
    <link href="{{asset('https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet">

    <style type="text/css">.jqstooltip {
            position: absolute;left: 0px;
            top: 0px;
            visibility: hidden;
            background: rgb(0, 0, 0);
            background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}
    </style>

</head>

<body class="fix-header">

<div class="preloader" style="display: none;">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle>
    </svg>
</div>

<div id="wrapper">

    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">
            <div class="top-left-part">

                <a class="logo" href="">

                    <b>
                        <img src="" alt="home" class="dark-logo">
                    </b>

                    <span class="hidden-xs">

	                        <img src="{{asset('img/textoamanecer.png')}}" alt="home" class="light-logo">
                    </span>
                </a>
            </div>
            <!-- /Logo -->
            <ul class="nav navbar-top-links navbar-right pull-right">

                <li>
                    <a class="profile-pic" href="">
                        {{--<img src="img/varun.jpg" alt="user-img" width="36" class="img-circle">--}}
                        <b class="hidden-xs">

                            @if(Auth::guard('web_administrador')->check())
                                Hola, {{ucfirst(Auth::guard('web_administrador')->user()->nombre)}}

                            @elseif(Auth::guard('web_estudiante')->check())
                                Hola, {{ucfirst(Auth::guard('web_estudiante')->user()->nombre)}}
                            @elseif(Auth::guard('web_profesor')->check())
                                Hola, {{ucfirst(Auth::guard('web_profesor')->user()->nombre)}}
                            @elseif(Auth::guard('web_secretaria')->check())
                                Hola, {{ucfirst(Auth::guard('web_secretaria')->user()->nombre)}}
                            @endif

                           </b>
                    </a>
                    </li>
                </ul>
            </div>

        </nav>

        <div class="navbar-default sidebar" role="navigation" style="overflow: visible;">
            <div class="slimScrollDiv" style="position: relative; overflow: visible; width: auto; height: 100%;">
                <div class="sidebar-nav slimscrollsidebar" style="overflow-x: visible; overflow-y: hidden; width: auto; height: 100%;">
                    <div class="sidebar-head">
                        <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3>
                    </div>
                    <ul class="nav in" id="side-menu">
                       @include('menu.index')

                    </ul>

                </div>
                <div class="slimScrollBar" style="background: rgba(0, 0, 0, 0.3); width: 6px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 533px;"></div>
                <div class="slimScrollRail" style="width: 6px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
            </div>

        </div>

        <div id="page-wrapper" style="min-height: 659px;">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

                        <h1 class="page-title">Panel Administrativo</h1> </div>

                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        @yield('boton')

                        <ol class="breadcrumb">

                        </ol>
                    </div>

                </div>




            </div>

            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">@yield('titulo')</h3>
                        @yield('contenido')
                    </div>
                </div>
            </div>



        </div>
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 © Fundacion Amanecer </footer>
    </div>

    </div>

    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/sidebar-nav.min.js')}}"></script>
    <script src="{{asset('js/jquery.slimscroll.js')}}"></script>
    <script src="{{asset('js/waves.js')}}"></script>
    <script src="{{asset('js/jquery.waypoints.js')}}"></script>
    <script src="{{asset('js/jquery.counterup.min.js')}}"></script>
    <script src="{{asset('js/custom.min.js')}}"></script>
    <script src="{{asset('js/jquery.toast.js')}}"></script>
    <script src="{{asset('https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/toastr.min.js')}}"></script>
    <script src="{{asset('https://unpkg.com/sweetalert/dist/sweetalert.min.js')}}"></script>

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
    @yield('script')

</body>
</html>