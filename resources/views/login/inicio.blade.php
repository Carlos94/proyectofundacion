<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Fundacion Amanecer-Inicio</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/logo.png')}}">
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
    <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Montserrat:400,700'>
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>

    <link rel="stylesheet" href="css/style2.css">


</head>

<body>

<div class="container">
    <div class="info">

    </div>
</div>
<div class="form">
    <div class=""><img src="img/logo.png"/></div>

    <form class="form-horizontal m-t-20" action="{{url('log')}}" method="POST">
        {{csrf_field()}}
        <input type="email" name="email" required="" placeholder="Correo electronico">
        <input type="password" name="password" required="" placeholder="Contraseña">
        <button>Ingresar</button>
    </form>
</div>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script  src="js/index.js"></script>

</body>
</html>