<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{asset('/dashboard/images/favicon.ico')}}">

    <!-- App title -->
    <title>Adminto - Responsive Admin Dashboard Template</title>

    <!-- App CSS -->
    <link href="{{asset('/dashboard/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/dashboard/css/core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/dashboard/css/components.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/dashboard/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/dashboard/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/dashboard/css/menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('/dashboard/css/responsive.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('/dashboard//js/modernizr.min.js')}}"></script>


</head>
<body>

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="text-center">
        <a href="index.html" class="logo"><span>Admin<span>to</span></span></a>
        <h5 class="text-muted m-t-0 font-600">Responsive Admin Dashboard</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Register</h4>
        </div>
        <div class="panel-body">
            <form class="form-horizontal m-t-20" action="index.html">

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="email" required="" placeholder="Email">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" placeholder="Username">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" required="" placeholder="Password">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-custom">
                            <input id="checkbox-signup" type="checkbox" checked="checked">
                            <label for="checkbox-signup">I accept <a href="#">Terms and Conditions</a></label>
                        </div>
                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">
                            Register
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>
    <!-- end card-box -->

    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="text-muted">Already have account?<a href="page-login.html" class="text-primary m-l-5"><b>Sign In</b></a></p>
        </div>
    </div>

</div>
<!-- end wrapper page -->




<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{'/dashboard/js/jquery.min.js'}}"></script>
<script src="{{'/dashboard/js/bootstrap.min.js'}}"></script>
<script src="{{'/dashboard/js/detect.js'}}"></script>
<script src="{{'/dashboard/js/fastclick.js'}}"></script>
<script src="{{'/dashboard/js/jquery.slimscroll.js'}}"></script>
<script src="{{'/dashboard/js/jquery.blockUI.js'}}"></script>
<script src="{{'/dashboard/js/waves.js'}}"></script>
<script src="{{'/dashboard/js/wow.min.js'}}"></script>
<script src="{{'/dashboard/js/jquery.nicescroll.js'}}"></script>
<script src="{{'/dashboard/js/jquery.scrollTo.min.js'}}"></script>

<!-- App js -->
<script src="{{'/dashboard/js/jquery.core.js'}}"></script>
<script src="{{'/dashboard/js/jquery.app.js'}}"></script>

</body>
</html>