
@if(Auth::guard('web_administrador')->check())

    <li style="padding: 70px 0 0;">
        <a href="/administrador" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Inicio</a>
    </li>
    <li>
        <a href="{{url('perfil-administrador')}}" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Perfil</a>
    </li>

    <li>
        <a href="/listado-secretarias" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Secretarias</a>
    </li>
    <li>
        <a href="/listado-profesores" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Profesores</a>
    </li>
    <li>
        <a href="/listado-de-cursos" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Cursos</a>
    </li>
    <li>
        <a href="/logout" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Cerrar Sesion</a>
    </li>



@elseif(Auth::guard('web_estudiante')->check())

@elseif(Auth::guard('web_profesor')->check())

    <li style="padding: 70px 0 0;">
        <a href="/profesor" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Inicio</a>
    </li>
    <li>
        <a href="perfil-profesor" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Perfil</a>
    </li>
    <li>
        <a href="/logout" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Cerrar Sesion</a>
    </li>

@elseif(Auth::guard('web_secretaria')->check())
    <li style="padding: 70px 0 0;">
        <a href="/secretaria" class="waves-effect"><i class="fa fa-clock-o fa-fw" aria-hidden="true"></i>Inicio</a>
    </li>
    <li>
        <a href="perfil-secretaria" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Perfil</a>
    </li>
    <li>
        <a href="logout" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Cerrar Sesion</a>
    </li>

@endif

