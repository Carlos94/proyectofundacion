@extends('layout.dashboard')

@section('titulo')
    Sus Cursos
@endsection

@section('contenido')


    <br>
    <div class="table-responsive">
        <table id="listado-estudiantes" class="table table-bordered">
            <thead>
            <tr>

                <th>Nombre del Curso.</th>
                <th>Duracion.</th>
                <th>Opciones.</th>

            </tr>
            </thead>
            @foreach($listadoCursos as $curso)
                <tr data-id="{{$curso->id}}">
                    <td>{{$curso->nombre}}</td>
                    <td>{{$curso->duracion}}</td>

                    <td>
                        {!! link_to_route('profesor/listado', $title ="VER ESTUDIANTES", $parameters = $curso->id,
                                                $attributes = ['class'=> 'btn  btn-danger'])!!}
                    </td>

                </tr>
            @endforeach
        </table>
    </div>
@endsection

@section('script')

    <script>

    </script>

@endsection

