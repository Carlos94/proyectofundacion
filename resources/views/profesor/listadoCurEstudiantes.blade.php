@extends('layout.dashboard')

@section('titulo')
    Listado de estudiantes
@endsection

@section('boton')
    <a type="button" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light"
       href="/profesor"  >Regresar</a>
@endsection

@section('contenido')

    @include('profesor.modals.modal-agregarNota')

    <div class="table-responsive">
        <table id="listado-estudiantes" class="table table-bordered">
            <thead>
            <tr>

                <th>Nombre.</th>
                <th>Apeliido</th>
                <th>Notas</th>

            </tr>
            </thead>
            @foreach($listas as $listado)
                <tr data-id="{{$listado->id}}">
                    <td>{{$listado->estudiante->nombre}}</td>
                    <td>{{$listado->estudiante->apellido}}</td>
                    <td>
                        <button type="button" class="btn btn-danger" onclick="abrir({{$listado->id}})">
                            Agregar Nota
                        </button>
                        {!! link_to_route('listNotas', $title ="Ver Notas", $parameters = $listado->id,
                       $attributes = ['class'=> 'btn  btn-danger'])!!}
                    </td>
            @endforeach

        </table>
    </div>
@endsection

@section('script')

    <script type="text/javascript">

        $(document).ready(function() {

            $('#listado-estudiantes').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"
                }
            });

        });


       function abrir(id)
       {
           $('#liNo').val(id);
           $("#modal-agregarNota").modal('toggle');
       }


    </script>

@endsection

