@extends('layout.dashboard')

@section('titulo')
    Datos del Estudiante
@endsection

@section('boton')


@endsection

@section('contenido')

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="nombre" >Nombre</label>
                <input type="text" id="nombre" name="nombre"  class="form-control" required="required"
                       value="{{$datosEstudiante->estudianteCurso->estudiante->nombre}}" disabled>
            </div>
            <div class="form-group">
                <label for="email" >Correo</label>
                <input type="text" id="email" name="email"  class="form-control" required="required"
                       value="{{$datosEstudiante->estudianteCurso->estudiante->email}}" disabled>
            </div>


        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label for="apellido" >Apellido </label>
                <input type="text" id="apellido" name="apellido" required="required"  class="form-control"
                       value="{{$datosEstudiante->estudianteCurso->estudiante->apellido}}" disabled>
            </div>

            <div class="form-group">
                <label for="contra" >Telefono</label>
                <input id="telefono" type="text" name="telefono"  class="form-control" value="{{$datosEstudiante->estudianteCurso->estudiante->telefono}}" disabled>
            </div>
        </div>

    </div>


    <hr>
    <h3 class="box-title">Notas</h3>
    <div class="table-responsive">
        <table id="listado-curProfesor" class="table table-bordered">
            <thead>
            <tr>

                <th>Descripcion</th>
                <th>Nota</th>

            </tr>
            </thead>
            @foreach($notasEstudiante as $nota)
                <tr data-id="{{$nota->id}}">
                    <td>{{$nota->descripcion}}</td>
                    <td>{{$nota->valor}}</td>

                </tr>
            @endforeach
        </table>
    </div>
@endsection


