<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="modal-agregarNota">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header" align="center" style="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                <h4 class="modal-title" id="myModalLabel">Agregar Nota</h4>
            </div>

            <form action="{{url('profesor/listado/notas')}}" method="POST" class="" autocomplete="off" id="registroNota">
                <div class="modal-body">

                    {{ csrf_field() }}

                    <input type="hidden" value="" id="liNo" name="liNo">
                    <div class="row">
                        <div class="form-group">
                            <label for="nombre" style="font-weight: 700">Descripcion</label>
                            <input  type="text" name="descripcion" id="descripcion"  class="form-control col-md-7 col-xs-12"
                                    placeholder="Descripcion de la nota" required>
                        </div>
                        <div class="form-group">
                            <label for="creditos">Valor</label>
                            <input type="text" class="form-control" id="valor" name="valor"  class="form-control col-md-7 col-xs-12"
                                   placeholder="Valor de la nota" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-info">Aceptar</button>
                </div>
            </form>


        </div>
    </div>
</div>

@push('script')
    <script type="text/javascript">
                $('#registroNota').on('submit', function (e) {
                    e.preventDefault();

                    $.ajax({
                        type: 'POST',
                        url: 'profesor/listado/notas',
                        data: $('#registroNota').serialize(),
                        success: function () {

                            swal("Tutorias", "Tu tutoria se ha creado con exito!!","success");

                            location.reload();

                        },
                        error: function (data) {
                            console.log(data.responseText);
                        }
                    });
                });
    </script>
@endpush